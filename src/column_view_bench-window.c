/* column_view_bench-window.c
 *
 * Copyright 2021 Ivan Molodetskikh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "column_view_bench-config.h"
#include "column_view_bench-window.h"

struct _ColumnViewBenchWindow
{
  GtkApplicationWindow  parent_instance;

  GtkColumnView *column_view;
  GtkColumnViewColumn *column_frame;
  GtkColumnViewColumn *column_time;
  GtkColumnViewColumn *column_ms;
  GtkColumnViewColumn *column_speed;
  GtkColumnViewColumn *column_vel_yaw;
  GtkColumnViewColumn *column_vert_speed;
  GtkColumnViewColumn *column_ground;
  GtkColumnViewColumn *column_duck_state;
  GtkColumnViewColumn *column_ladder_state;
  GtkColumnViewColumn *column_water_level;
  GtkColumnViewColumn *column_jump;
  GtkColumnViewColumn *column_duck;
  GtkColumnViewColumn *column_forward;
  GtkColumnViewColumn *column_side;
  GtkColumnViewColumn *column_up;
  GtkColumnViewColumn *column_yaw;
  GtkColumnViewColumn *column_pitch;
  GtkColumnViewColumn *column_health;
  GtkColumnViewColumn *column_armor;
  GtkColumnViewColumn *column_use;
  GtkColumnViewColumn *column_attack_1;
  GtkColumnViewColumn *column_attack_2;
  GtkColumnViewColumn *column_reload;
  GtkColumnViewColumn *column_client_state;
  GtkColumnViewColumn *column_shared_seed;
  GtkColumnViewColumn *column_pos_z;
  GtkColumnViewColumn *column_pos_x;
  GtkColumnViewColumn *column_pos_y;
  GtkColumnViewColumn *column_remainder;
};

G_DEFINE_TYPE (ColumnViewBenchWindow, column_view_bench_window, GTK_TYPE_APPLICATION_WINDOW)

static void
column_view_bench_window_class_init (ColumnViewBenchWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/com/example/ColumnViewBench/column_view_bench-window.ui");
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_view);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_frame);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_time);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_ms);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_speed);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_vel_yaw);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_vert_speed);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_ground);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_duck_state);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_ladder_state);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_water_level);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_jump);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_duck);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_forward);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_side);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_up);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_yaw);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_pitch);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_health);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_armor);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_use);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_attack_1);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_attack_2);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_reload);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_client_state);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_shared_seed);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_pos_z);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_pos_x);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_pos_y);
  gtk_widget_class_bind_template_child (widget_class, ColumnViewBenchWindow, column_remainder);
}

/* This is our dummy item for the model. */
#define COLUMN_VIEW_BENCH_TYPE_ITEM (column_view_bench_item_get_type ())
G_DECLARE_FINAL_TYPE (ColumnViewBenchItem, column_view_bench_item, COLUMN_VIEW_BENCH, ITEM, GObject)

struct _ColumnViewBenchItem
{
  GObject parent_instance;
  int data;
};

struct _ColumnViewBenchItemClass
{
  GObjectClass parent_class;
};

G_DEFINE_TYPE (ColumnViewBenchItem, column_view_bench_item, G_TYPE_OBJECT)

static void
column_view_bench_item_init (ColumnViewBenchItem *item)
{
}

static void
column_view_bench_item_class_init (ColumnViewBenchItemClass *class)
{
}

static ColumnViewBenchItem *
column_view_bench_item_new (int data)
{
  ColumnViewBenchItem *item = g_object_new (COLUMN_VIEW_BENCH_TYPE_ITEM, NULL);
  item->data = data;
  return item;
}

static void
setup (GtkSignalListItemFactory *factory,
       GObject                  *listitem)
{
  GtkWidget *label;
  label = gtk_label_new ("");
  gtk_list_item_set_child (GTK_LIST_ITEM (listitem), label);
}

static void
bind (GtkSignalListItemFactory *factory,
      GObject                  *listitem)
{
  GtkWidget *widget;
  GObject *item;

  /* Retrieving the widget and the item is something one will likely want to do
   * in every bind () implementation. */
  widget = gtk_list_item_get_child (GTK_LIST_ITEM (listitem));
  item = gtk_list_item_get_item (GTK_LIST_ITEM (listitem));

  /* The actual work here is commented out for the sake of benchmarking just
   * the column view. */

  /* char buffer[16] = { 0, }; */
  /* g_snprintf (buffer, sizeof (buffer), "%d", COLUMN_VIEW_BENCH_ITEM (item)->data); */
  /* gtk_label_set_label (GTK_LABEL (widget), buffer); */
}

static void
column_view_bench_window_init (ColumnViewBenchWindow *self)
{
  GtkListItemFactory *factory;

  gtk_widget_init_template (GTK_WIDGET (self));

  /* Create and bind a separate factory to each column as one would likely want to do. */
  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_frame, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_time, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_ms, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_speed, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_vel_yaw, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_vert_speed, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_ground, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_duck_state, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_ladder_state, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_water_level, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_jump, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_duck, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_forward, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_side, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_up, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_yaw, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_pitch, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_health, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_armor, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_use, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_attack_1, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_attack_2, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_reload, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_client_state, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_shared_seed, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_pos_z, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_pos_x, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_pos_y, factory);
  g_object_unref (factory);

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind), NULL);
  gtk_column_view_column_set_factory (self->column_remainder, factory);
  g_object_unref (factory);

  /* Add our set of dummy items. */
  GListStore *store = g_list_store_new (G_TYPE_OBJECT);
  for (int i = 0; i < 10000; ++i)
    {
      ColumnViewBenchItem *item = column_view_bench_item_new (i);
      g_list_store_append (store, item);
      g_object_unref (item);
    }

  GtkMultiSelection *selection = gtk_multi_selection_new (G_LIST_MODEL (store));
  gtk_column_view_set_model (self->column_view, GTK_SELECTION_MODEL (selection));
}
